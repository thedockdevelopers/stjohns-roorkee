import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from "rxjs";
import { AuthService } from "../services/auth.service";
import { UtilityService } from "../services/utility.service";
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-signup-form',
    templateUrl: 'signup.component.html',
    styleUrls: ['signup.component.scss']
})

export class Signup implements OnInit, OnDestroy {
    showSpinner: boolean;
    errorMsg: string;
    isSubmitted: boolean;
    signupErrorObs: Subscription;
    userDataObs: Subscription;
    roles = ['Student', 'Teacher'];
    isAdmin : boolean;
    user: any;
    formValue: any;
    signupSuccessObs: Subscription;
    userPostObs: Subscription;
    signupForm = new FormGroup({
        username: new FormControl('', Validators.required),
        email: new FormControl('', Validators.compose([Validators.required,
        Validators.pattern(RegularExpressionConstant.EMAIL)])),
        password: new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(8)])),
        //adminpassword: new FormControl(''),
        role: new FormControl('', Validators.required)
    });
    constructor(public dialogRef: MatDialogRef<Signup>,
        private utilityService: UtilityService,
        private authService: AuthService,
        public toastr: ToastrService) {

    }

    ngOnInit() {
        this.signupErrorObs = this.authService.signupErrorObservable.subscribe((error: any) => {
            if(error && error.message) {
                this.showSpinner = false;
                this.errorMsg = error.message
                if(this.errorMsg.includes(':')) {
                    this.errorMsg = this.errorMsg.split(':')[1];
                }
            }
        });

        this.userPostObs = this.utilityService.userDataPostObservable.subscribe((result: string)=> {
            if(result == 'stored') {
                this.authService.signUp(this.formValue);
            }
            else if(result == 'notstored') {
                this.errorMsg = 'We are facing some technical issues. Please try again after sometime';
            }
        });
        // this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
        //     if(user && user.email && this.signupForm && (user.email == this.signupForm.controls['email'].value)) {
        //         this.alreadyRegistered = true;
        //         this.user = user;
        //         this.errorMsg = 'Email address is already in use. Please login or try with other email address.';
        //     }
        // });
        this.signupSuccessObs = this.authService.authSuccessObservable.subscribe((result: any) => {
            if (result) {
                this.showSpinner = false;
                this.toastr.success('You are successfully registered', 'Success', {
                    timeOut: 3000,
                    closeButton: true
                },
                );
                this.closeDialog();
            }
        });
    }
    
    
    get formControls() {
        return this.signupForm['controls'];
    }

    resetForm() {
        this.signupForm.setValue({
            username: '',
            email: '',
            password: '',
            role: ''
            //adminpassword: ''
        });
        this.isSubmitted = false;
        this.errorMsg = '';
        this.showSpinner = false;
    }

    closeDialog() {
        this.showSpinner = false;
        this.resetForm();
        this.dialogRef.close();
    }

    changeRole(e) {
        this.signupForm.controls['role'].setValue(e.target.value);
        let roleValue = this.signupForm.controls['role'].value;
        // if (roleValue.includes('Admin')) {
        //     this.isAdmin = true;
        // }
        // else {
        //     this.isAdmin = false;
        // }
      }

    // emailChanged(evt: any) {
    //     if (evt.target.value) {
    //         this.utilityService.getUserDetailsFromDatabas(evt.target.value);
    //     }
    // }

    signup(formValue: any) {
        this.isSubmitted = true;
        this.errorMsg = '';
        if (this.signupForm.valid) {
            this.showSpinner = true;
            formValue['date'] = this.utilityService.getCurrentDate();
            formValue['time'] = this.utilityService.getcurrentTime();
            this.formValue = formValue;
            this.utilityService.insertUserDetailsIntoDatabase(formValue);
            // this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
            //     if (user && user!='doesnotexist') {
            //         this.isError = true;
            //         if(user == 'usernameexist') {
            //             alert('Username already exist')
            //         }
            //         else {
            //             alert('Account is already associated with provided email address');
            //         }
            //     }
            //     else if(user == 'doesnotexist') {
            //         this.utilityService.insertUserDetailsIntoDatabase(formValue);
            //         this.authService.signUp(formValue);
            //         this.userDataObs.unsubscribe();
            //         this.closeDialog();
            //         alert('User created');
            //     }
            // });
        }
    }

    ngOnDestroy() {
        if (this.signupErrorObs) {
            this.signupErrorObs.unsubscribe();
        }
        if (this.userDataObs) {
            this.userDataObs.unsubscribe();
        }
        if(this.signupSuccessObs) {
            this.signupSuccessObs.unsubscribe();
        }
    }
}

export class RegularExpressionConstant {
    static EMAIL: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}