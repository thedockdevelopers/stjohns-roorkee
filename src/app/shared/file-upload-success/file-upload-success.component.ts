import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from "@angular/router";

@Component({
    selector: 'app-file-upload-success',
    templateUrl: 'file-upload-success.component.html',
    styleUrls: ['file-upload-success.component.scss']
})

export class FileUploadSuccess implements OnInit {
    fileName: string;
    fileDesc: string;
    fileUrl: string;
    fileRoute: string;
    copyButtonText = 'Copy Link';
    copied: boolean;

    constructor(public dialogRef: MatDialogRef<File>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public router: Router) {
    }

    ngOnInit() {
        let host = window.location.protocol + "//" + window.location.host;
        this.fileName = this.data ? this.data.name : 'Error';
        this.fileDesc = this.data ? this.data.desc : 'We are experiencing a problem at this moment. Please try again after sometime';
        this.fileUrl = this.data ? this.data.url : '';
        this.fileRoute = this.data ? host + '/download/' + this.data.id : '';
    }

    closeDialog() {
        this.dialogRef.close();
    }

    copyLink() {
        let copyText = document.getElementById('linkText');
        let selection = window.getSelection();
        let range = document.createRange();
        range.selectNodeContents(copyText);
        selection.removeAllRanges();
        selection.addRange(range);
        document.execCommand('Copy');
        this.copied = true;
        this.copyButtonText = 'Copied';
    }
}