import { Component, ElementRef, Inject, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Component({
    selector: 'app-file-form',
    templateUrl: 'file.component.html',
    styleUrls: ['file.component.scss']
})

export class File implements OnInit {
    fileName: string;
    fileDesc: string;
    fileUrl: string;
    fileRoute: string;
    copyButtonText = 'Copy Link';
    copied: boolean;

    constructor(public dialogRef: MatDialogRef<File>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public router: Router) {
    }

    ngOnInit() {
        let host = window.location.protocol + "//" + window.location.host;
        this.fileName = this.data ? this.data.name : 'Not Found';
        this.fileDesc = this.data ? this.data.desc : 'We didn\'t find any file with the provided ID';
        this.fileUrl = this.data ? this.data.url : '';
        this.fileRoute = this.data ? (host + '/download/') + this.data.id : '';
    }

    closeDialog() {
        this.dialogRef.close();
        this.router.navigate(['download']);
    }

    copyLink() {
        let copyText = document.getElementById('linkText');
        let selection = window.getSelection();
        let range = document.createRange();
        range.selectNodeContents(copyText);
        selection.removeAllRanges();
        selection.addRange(range);
        document.execCommand('Copy');
        this.copied = true;
        this.copyButtonText = 'Copied';
    }

    // navigateToDownload() {
    //     this.router.navigate(['download']);
    // }
}