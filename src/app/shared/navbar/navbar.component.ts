import { Component, OnInit, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { MatDialog } from '@angular/material/dialog';
import { Login } from '../login/login.component';
import { Signup } from '../signup/signup.component';
import { AuthService } from '../services/auth.service';
import { UtilityService } from '../services/utility.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;
    userData: any;
    userAuthObs: Subscription;
    userDataObs: Subscription;
    isLoggedIn: boolean;

    constructor(public location: Location, private element: ElementRef,
        private angularFireAuth: AngularFireAuth,
        private dialog: MatDialog,
        private authService: AuthService,
        private utilityService: UtilityService) {
        this.sidebarVisible = false;
        this.userAuthObs = this.angularFireAuth.authState.subscribe(user => {
            if (user && user.email) {
                this.isLoggedIn = true;
                this.utilityService.getUserDetailsFromDatabase(user.email);
            }
            else {
                this.isLoggedIn = false;
            }
        });
        this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
            if(user) {
                this.userData = user;
            }
        });
    }

    ngOnInit() {
        window.onclick = function(event) {
            if (!event.target.matches('.dropdown-btn')) {
              var dropdowns = document.getElementsByClassName("dropdown-content");
              var i;
              for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                  openDropdown.classList.remove('show');
                }
              }
            }
          }
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        // console.log(toggleButton, 'toggle');

        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };
    isHome() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee === '/home') {
            return true;
        }
        else {
            return false;
        }
    }
    isDownload() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee === '/download') {
            return true;
        }
        else {
            return false;
        }
    }
    isFileDownload() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if( titlee.includes('/download/' ) ) {
            return true;
        }
        else {
            return false;
        }
    }
    isDocumentation() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        if (titlee === '/documentation') {
            return true;
        }
        else {
            return false;
        }
    }

    scrollToUpload() {
        this.sidebarToggle();
        document.getElementById("upload").scrollIntoView({
            behavior: "smooth",
            block: "start",
            inline: "nearest"
        });
    }

    openSignupDialog() {
        this.sidebarToggle();
        this.dialog.open(Signup, {
            height: '100vh',
            width: '100vw',
            panelClass: 'mat-dialog-modal'
        });
    }

    opeLoginDialog() {
        this.sidebarToggle();
        this.dialog.open(Login, {
            height: '100vh',
            width: '100vw',
            panelClass: 'mat-dialog-modal'
        });
    }

    logout() {
        this.sidebarToggle();
        this.authService.signOut();
    }

    openProfile() {
        document.getElementById("userProfile").classList.toggle("show");
}
}
