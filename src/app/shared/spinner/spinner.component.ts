import { Component, Input, OnChanges } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-spinner-form',
    templateUrl: 'spinner.component.html',
    styleUrls: ['spinner.component.scss']
})

export class Spinner implements OnChanges {
    @Input() show: boolean;
    @Input() color: string;
    constructor(private spinner: NgxSpinnerService) {
    }

    ngOnChanges() {
        if(this.show) {
            this.spinner.show();
        }
        else {
            this.spinner.hide();
        }
    }
}