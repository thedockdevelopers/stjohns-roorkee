import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Spinner } from './spinner.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
    imports: [
        CommonModule,
        NgxSpinnerModule
    ],
    declarations: [
        Spinner
    ],
    entryComponents: [Spinner],
    exports:[ Spinner ]
})
export class SpinnerModule { }
