import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { AuthService } from "../services/auth.service";
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from "@angular/material/dialog";

@Component({
    selector: 'app-login-form',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class Login implements OnInit {
    showSpinner: boolean;
    isSubmitted: boolean;
    errorMessage: any;
    loginErrorObs: Subscription;
    loginSuccessObs: Subscription;
    loginForm = new FormGroup({
        email: new FormControl('', Validators.compose([Validators.required, 
            Validators.pattern(RegularExpressionConstant.EMAIL)])),
        password: new FormControl('', Validators.compose([Validators.required, 
            Validators.minLength(8)]))
    });
    constructor(
        private authService: AuthService,
        public toastr: ToastrService,
        private dialogRef: MatDialogRef<Login>) {
    }

    ngOnInit() {
        this.loginErrorObs = this.authService.loginErrorObservable.subscribe((error: any) => {
            if(error && error.message) {
                this.showSpinner = false;
                this.errorMessage = error.message;
                if(this.errorMessage.includes(':')) {
                    this.errorMessage = this.errorMessage.split(':')[1];

                }
            }
        });
        this.loginSuccessObs = this.authService.authSuccessObservable.pipe().subscribe((result: any) => {
            if (result) {
                this.showSpinner = false;
                this.toastr.success('You are successfully logged in', 'Success', {
                    timeOut: 3000,
                    closeButton: true
                },
                );
                this.closeDialog();
            }
        });
    }

    get formControls() {
        return this.loginForm['controls'];
    }

    // showPassword() {
    //     var password = this.loginForm.controls['password'].;
    //     if (x.type === "password") {
    //       x.type = "text";
    //     } else {
    //       x.type = "password";
    //     }
    //   }

    resetForm() {
        this.loginForm.reset();
        this.loginForm.setValue({
            email: '',
            password: ''
        });
        this.isSubmitted = false;
        this.errorMessage = false;
        this.showSpinner = false;
    }

    closeDialog() {
        this.resetForm();
        this.dialogRef.close();
    }

    login(formValue: any) {
        this.isSubmitted = true;
        this.errorMessage = false;
        if (this.loginForm.valid) {
            this.showSpinner = true;
            this.authService.signIn(formValue);
        }
    }

    ngOnDestroy() {
        if (this.loginErrorObs) {
            this.loginErrorObs.unsubscribe();
        }
    }
}

export class RegularExpressionConstant {
    static EMAIL: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}