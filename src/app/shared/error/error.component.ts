import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from "@angular/router";

@Component({
    selector: 'app-error-form',
    templateUrl: 'error.component.html',
    styleUrls: ['error.component.scss']
})

export class Error implements OnInit {
    fileName: string;
    fileDesc: string;
    fileUrl: string;

    constructor(public dialogRef: MatDialogRef<File>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public router: Router) {
    }

    ngOnInit() {
        this.fileName = this.data ? this.data.name : 'Not Found';
        this.fileDesc = this.data ? this.data.desc : 'We didn\'t find any file with the provided ID';
        this.fileUrl = this.data ? this.data.url : '';
    }

    closeDialog() {
        this.dialogRef.close();
        this.router.navigate(['download']);
    }
}