export class ConstantService {
    globalErrorMsg = 'We are facing some issue, please try again after sometime';
    notLoggedInErrorMsg = 'You are not logged in';
    loggedInSuccessMsg = 'You are successfully logged in';
    signUpSuccessMsg = 'You are successfully registered. Please login.';
    nonLoggedInFileUploadErrorMsg = 'Please login to upload file'
}