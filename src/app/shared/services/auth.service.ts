import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;
  private loginErrorSubject = new BehaviorSubject<any>(null);
  loginErrorObservable: Observable<any> = this.loginErrorSubject.asObservable();
  private logoutErrorSubject = new BehaviorSubject<any>(null);
  logoutErrorObservable: Observable<any> = this.logoutErrorSubject.asObservable();
  private signupErrorSubject = new BehaviorSubject<any>(null);
  signupErrorObservable: Observable<any> = this.signupErrorSubject.asObservable();
  private authSuccessSubject = new BehaviorSubject<boolean>(null);
  authSuccessObservable: Observable<boolean> = this.authSuccessSubject.asObservable();

  constructor(
    private angularFireAuth: AngularFireAuth,
    public router: Router,  
    public ngZone: NgZone,
    public toastr: ToastrService
  ) {
    
  }

  signIn(formvalue: any) {
    return this.angularFireAuth.signInWithEmailAndPassword(formvalue.email, formvalue.password)
      .then((result) => {
        if(result){
        this.ngZone.run(() => {
          this.authSuccessSubject.next(true);
        });
      }
      }).catch((error) => {
        this.loginErrorSubject.next(error);
      });
  }

  signOut() {
    return this.angularFireAuth.signOut().then((result: any) => {
      this.toastr.success('You are successfully logged out', 'Success', {
        timeOut: 3000,
        closeButton: true
      });
      window.location.reload();
    }).catch((error) => {
      this.toastr.error('We are facing a problem at this moment. Please try again', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
    })
  }

  signUp(formValue:any) {
    return this.angularFireAuth.createUserWithEmailAndPassword(formValue.email, formValue.password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign 
        up and returns promise */
        // this.SendVerificationMail();
        // this.SetUserData(result.user);
        if(result) {
          this.authSuccessSubject.next(true);
        }
      }).catch((error) => {
        this.signupErrorSubject.next(error);
      })
  }
}
