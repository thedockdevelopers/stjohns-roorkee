import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileUploadSuccess } from '../file-upload-success/file-upload-success.component';

@Injectable({
  providedIn: 'root'
})
export class UtilityService implements OnDestroy {
  fileDetailList: AngularFireList<any>;
  private userSubject = new BehaviorSubject<any>(null);
  userObservable: Observable<boolean> = this.userSubject.asObservable();
  firestoreSubscription: Subscription;
  private fileUploadSubject = new BehaviorSubject<any>(null);
  fileUploadObservable: Observable<any> = this.fileUploadSubject.asObservable();
  private userDataPostSubject = new BehaviorSubject<any>(null);
  userDataPostObservable: Observable<any> = this.userDataPostSubject.asObservable();
  private documentIdsSubject = new BehaviorSubject<any>(null);
  documentIdsObservable: Observable<any> = this.documentIdsSubject.asObservable();

  constructor(
    //private angularFireDatabase: AngularFireDatabase,
    private angularFirestore: AngularFirestore,
    private dialog: MatDialog
  ) { }

  insertFileDetailsIntoDatabase(fileDetails: any) {
    // let fireDatabaseFileList = this.angularFireDatabase.list('documents');
    // fireDatabaseFileList.push(fileDetails);
    this.angularFirestore.collection('documents').doc(fileDetails.id).set(fileDetails).then(() => {
      this.fileUploadSubject.next('uploaded');
      this.dialog.open(FileUploadSuccess, {
        height: '100vh',
        width: '100vw',
        panelClass: 'mat-dialog-modal',
        data: fileDetails
      });
    },
    (error) => {
      this.fileUploadSubject.next('failed');
    })
  }

  getFileDetailsFromDatabase(): Observable<any> {
    return this.angularFirestore.collection('documents').valueChanges();
    // return this.angularFirestore.collection('documents').snapshotChanges().subscribe((fileList) => {
    //   if(fileList) {
    //     console.log(fileList.entries());
    //   }
    // });
  }

  // getUserDetailsFromDatabase(email: string, username: string) {
  //   this.angularFirestore.collection('users').valueChanges().subscribe(users => {
  //     if(users) {
  //       let emailCounter = 0;
  //       let usernameCounter = 0;
  //       users.forEach((user: any) => {
  //         if(user.email == email) {
  //           emailCounter++;
  //           this.userSubject.next(user);
  //           return;
  //         }
  //       })
  //       users.forEach((user: any) => {
  //         if(user.username == username) {
  //           usernameCounter++;
  //           this.userSubject.next('usernameexist');
  //           return;
  //         }
  //       })
  //       if(emailCounter == 0 && usernameCounter == 0) {
  //         this.userSubject.next('doesnotexist');
  //       }
  //     }
  //   });
  // }

  getUserDetailsFromDatabase(email: string) {
    this.firestoreSubscription = this.angularFirestore.collection('users').doc(email).valueChanges().subscribe((user:any) => {
      if(user) {
        this.userSubject.next(user);
      }
    });
  }

  getDocumentIds(id: any){
    return this.angularFirestore.collection('documents').doc(id).valueChanges().subscribe((document: any) => {
      if(document) {
        this.documentIdsSubject.next('existed');
      }
      else {
        this.documentIdsSubject.next('notexisted');
      }
    });
  }

  insertUserDetailsIntoDatabase(userDetails: any) {
    this.angularFirestore.collection('users').doc(userDetails.email).set(userDetails).then(()=>{
      this.userDataPostSubject.next('stored');
    },(error)=>{
      this.userDataPostSubject.next('notstored');
    });
  }

  getCurrentDate() {
    let today = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1;
    let yyyy: any = today.getFullYear();

    if (dd < 10) {
      dd = '0' + String(dd);
    }
    if (mm < 10) {
      mm = '0' + String(mm);
    }
    let currentDate = dd + '/' + mm + '/' + String(yyyy);
    return currentDate;
  }

  getcurrentTime() {
    let today = new Date();
    let hh = String(today.getHours());
    let mm = String(today.getMinutes());
    let ss = String(today.getSeconds());
    return hh+':'+mm+':'+ss;
  }

  ngOnDestroy() {
    if(this.firestoreSubscription) {
      this.firestoreSubscription.unsubscribe();
    }
  }

  getAdminAcessCode(): Observable<any> {
    return this.angularFirestore.collection('adminaccessusers').valueChanges();
  }

}
