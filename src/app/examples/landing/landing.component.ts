import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { File } from 'app/shared/file/file.component';
import { Login } from 'app/shared/login/login.component';
import { UtilityService } from 'app/shared/services/utility.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {
  showSpinner: boolean;
  focus: any;
  focus1: any;
  firebaseDBObs: Observable<any>;
  documentArray: any;

  constructor(
    private utilityService: UtilityService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router) {
  }

  ngOnInit() {
    this.showSpinner = true;
    let fileId = this.activatedRoute.snapshot.paramMap.get('fileId');
    this.firebaseDBObs = this.utilityService.getFileDetailsFromDatabase();
    this.firebaseDBObs.subscribe((fileList) => {
      this.documentArray = fileList;
      this.showSpinner = false;
      if (fileId) {
        let fileData: any;
        fileList.forEach((file: any) => {
          if (file.id == fileId) {
            fileData = file;
          }
        });
        this.dialog.open(File, {
          height: '100vh',
          width: '100vw',
          panelClass: 'mat-dialog-modal',
          data: fileData
        });
      }
    });
  }

  openFile(fileId) {
    this.router.navigate(['download/'+fileId]);
  }

}
