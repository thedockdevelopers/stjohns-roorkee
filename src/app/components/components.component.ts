import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage } from '@angular/fire/compat/storage'
import { finalize } from 'rxjs/operators';
import { UtilityService } from '../shared/services/utility.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ConstantService } from '../shared/services/const.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styles: [`
    ngb-progressbar {
        margin-top: 5rem;
    }
    `]
})

export class ComponentsComponent implements OnInit {
    showSpinner: boolean;
    page = 4;
    page1 = 5;
    focus;
    focus1;
    focus2;
    date: { year: number, month: number };
    model: NgbDateStruct;
    isUploaded: boolean;
    selectedFile: any;
    userData: any;
    isError: boolean;
    errorMsg: string;
    isLoggedIn: boolean;
    userAuthObs: Subscription;
    userDataObs: Subscription;
    fileUploadObs: Subscription;
    accessCodeObs: Subscription
    docuemntIdObs: Subscription;
    formValue: any;

    formTemplate = new FormGroup({
        name: new FormControl('', Validators.required),
        id: new FormControl('', Validators.required),
        desc: new FormControl('', Validators.required),
        adminacesscode: new FormControl('', Validators.required),
        url: new FormControl('', Validators.required)
    });

    constructor(private utilityService: UtilityService,
        private angularFireStorage: AngularFireStorage,
        private angularFireAuth: AngularFireAuth,
        private constantService: ConstantService) {
        this.userAuthObs = this.angularFireAuth.authState.subscribe(user => {
            if (user && user.email) {
                this.isLoggedIn = true;
                this.errorMsg = '';
                this.utilityService.getUserDetailsFromDatabase(user.email);
            }
            else {
                this.isLoggedIn = false;
                this.errorMsg = this.constantService.nonLoggedInFileUploadErrorMsg;
                this.isDisabled(this.userData);
            }
        });
        this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
            if(user) {
                this.userData = user;
                this.isDisabled(this.userData);
            }
        });

        this.docuemntIdObs = this.utilityService.documentIdsObservable.subscribe((result: any) => {
            if (this.userData && this.formTemplate.valid && this.isUploaded) {
                if (result == 'existed') {
                    this.showSpinner = false;
                    this.errorMsg = 'Document Id already exist. Please enter unique Document Id.';
                }
                else if (result = 'notexisted') {
                    this.showSpinner = true;
                    this.errorMsg = '';
                    this.processAdminAccessCode(this.formValue);
                }
            }
        });

        this.fileUploadObs = this.utilityService.fileUploadObservable.subscribe((result: string) => {
            if(result == 'uploaded') {
                this.showSpinner = false;
                this.errorMsg = '';
            }
            else if(result == 'failed') {
                this.showSpinner = false;
                this.errorMsg = 'File upload failled due to technical error. Please try again later.'
            }
        })
    }

    isWeekend(date: NgbDateStruct) {
        const d = new Date(date.year, date.month - 1, date.day);
        return d.getDay() === 0 || d.getDay() === 6;
    }

    // isDisabled(date: NgbDateStruct, current: { month: number }) {
    //     return date.month !== current.month;
    //}

    ngOnInit() {
        let input_group_focus = document.getElementsByClassName('form-control');
        let input_group = document.getElementsByClassName('input-group');
        for (let i = 0; i < input_group.length; i++) {
            input_group[i].children[0].addEventListener('focus', function () {
                input_group[i].classList.add('input-group-focus');
            });
            input_group[i].children[0].addEventListener('blur', function () {
                input_group[i].classList.remove('input-group-focus');
            });
        }
        this.resetForm();
    }

    scrol() {
        document.getElementById("main").scrollIntoView({
            behavior: "smooth",
            block: "start",
            inline: "nearest"
        });
    }

    fileChanged(event: any) {
        if (event.target.files && event.target.files[0]) {
            this.selectedFile = event.target.files[0];
        }
    }

    get formControls() {
        return this.formTemplate['controls'];
    }

    isDisabled(value) {
        if(!value) {
         this.formTemplate.controls['name'].disable();
         this.formTemplate.controls['id'].disable();
         this.formTemplate.controls['adminacesscode'].disable();
         this.formTemplate.controls['desc'].disable();
         this.formTemplate.controls['url'].disable();
        }
        else {
            this.formTemplate.controls['name'].enable();
            this.formTemplate.controls['id'].enable();
            this.formTemplate.controls['adminacesscode'].enable();
            this.formTemplate.controls['desc'].enable();
            this.formTemplate.controls['url'].enable();
        }
       }
    //function to upload files to firebase cloud storage
    upload(formValue: any) {
        this.isUploaded = true;
        if (this.formTemplate.valid) {
            this.showSpinner = true;
            this.formValue = formValue;
            this.utilityService.getDocumentIds(formValue['id']);            
        }
    }

    processAdminAccessCode(formValue: any) {
        this.accessCodeObs = this.utilityService.getAdminAcessCode().subscribe((result: any)=>{
            if(result && result.length > 0) {
                for(let i=0; i< result.length; i++){
                    if(result[i].accesscode == formValue['adminacesscode']) {
                        this.processFileUpload(formValue);
                        return false;
                    }
                }
                this.showSpinner = false;
                this.errorMsg = 'Please enter correct access code. Contact school administration to get your access code.';
            }
        });
    }

    processFileUpload(formValue: any) {
        this.showSpinner = true;
        var filePath = `documents/${formValue.id}_${this.selectedFile.name}_${(new Date().getTime())}`;
        let fileRef = this.angularFireStorage.ref(filePath);
        this.angularFireStorage.upload(filePath, this.selectedFile).snapshotChanges().pipe(
            finalize(() => {
                fileRef.getDownloadURL().subscribe((url: any) => {
                    formValue['url'] = url;
                    formValue['date'] = this.utilityService.getCurrentDate();
                    formValue['time'] = this.utilityService.getcurrentTime();
                    formValue['email'] = this.userData?.email;
                    formValue['username'] = this.userData?.username;
                    this.utilityService.insertFileDetailsIntoDatabase(formValue);
                    this.resetForm();
                });
            })
        ).subscribe((result)=>{
        },(error) => {
           // this.showSpinner = false;
            this.errorMsg = error.message;
            if(this.errorMsg.includes(':')) {
                this.errorMsg = this.errorMsg.split(':')[1];

            }
        });
    }

    resetForm() {
        this.formTemplate.reset();
        this.formTemplate.setValue({
            name: '',
            id: '',
            desc: '',
            adminacesscode: '',
            url: '',
        });
        this.isUploaded = false;
        this.selectedFile = null;
    }

    ngOnDestroy() {
        if(this.userAuthObs) {
            this.userAuthObs.unsubscribe();
        }
        if(this.userDataObs) {
            this.userDataObs.unsubscribe();
        }
        if(this.fileUploadObs) {
            this.fileUploadObs.unsubscribe();
        }
        if(this.accessCodeObs) {
            this.accessCodeObs.unsubscribe();
        }
    }
}
